package AddAbstract;

import java.util.Scanner;

public abstract class X {

	    int a, b, sum;

	    abstract void input();
	    abstract void add();
	    abstract void result();
	    Scanner sc = new Scanner(System.in);
	}

public class Y extends class X{

	    void input() {
	        System.out.print("Enter Two Numbers :");
	        a = sc.nextInt();
	        b = sc.nextInt();
	    }

	    void add() {
	        sum = a + b;
	    }

	    void result() {
	        System.out.print("The Sum of two numbers is :" + sum);
	    }

	    public static void main(String args[]) {
	        Y s = new Y();
	        s.input();
	        s.add();
	        s.result();
	    }
	}

